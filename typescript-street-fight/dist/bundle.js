/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/dist/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./index.js":
/*!******************!*\
  !*** ./index.js ***!
  \******************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var app_1 = __webpack_require__(/*! ./src/typescript/app */ "./src/typescript/app.js");

__webpack_require__(/*! ./src/styles/styles.css */ "./src/styles/styles.css");

app_1.startApp();

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/styles/styles.css":
/*!*********************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/styles/styles.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "html,\nbody {\n    height: 100%;\n    width: 100%;\n    margin: 0;\n    padding: 0;\n}\n\n#root {\n    display: flex;\n    flex-direction: column;\n    align-items: center;\n    justify-content: center;\n    height: 100%;\n    width: 100%;\n}\n\n.fighters {\n    display: flex;\n    justify-content: space-between;\n    align-items: center;\n    flex: 1;\n    flex-wrap: wrap;\n    padding: 0 15px;\n}\n\n.fighter {\n    display: flex;\n    flex-direction: column;\n    padding: 20px;\n}\n\n.fighter:hover {\n    box-shadow: 0 0 50px 10px rgba(0,0,0,0.06);\n    cursor: pointer;\n}\n\n.name {\n    align-self: center;\n    font-size: 21px;\n    margin-top: 20px;\n}\n\n.fighter-image {\n    height: 260px;\n}\n\n#loading-overlay {\n    position: absolute;\n    top: 0;\n    left: 0;\n    width: 100%;\n    height: 100%;\n    display: flex;\n    align-items: center;\n    justify-content: center;\n    font-size: 18px;\n    background: rgba(255, 255, 255, 0.7);\n    visibility: hidden;\n}\n\n.modal-layer {\n    position: absolute;\n    top: 0;\n    left: 0;\n    height: 100%;\n    width: 100%;\n    display: flex;\n    align-items: center;\n    justify-content: center;\n    background-color: rgba(128,128,128,0.6);\n}\n\n.modal-root {\n    display: flex;\n    flex-direction: column;\n    border: 1px solid rgba(0,0,0,.2);\n    border-radius: .3rem;\n    background-color: white;\n}\n\n.modal-header {\n    display: flex;\n    align-items: center;\n    justify-content: space-between;\n    min-width: 300px;\n    padding: 1rem;\n    border-bottom: 1px solid #e9ecef;\n    border-top-left-radius: .3rem;\n    border-top-right-radius: .3rem;\n    font-weight: 700;\n    font-size: 22px;\n}\n\n.modal-body {\n    padding: 1rem;\n}\n\n.close-btn {\n    font-size: 1.5rem;\n    font-weight: 700;\n    line-height: 1;\n    cursor: pointer;\n}\n\n.custom-checkbox {\n    display: block;\n    position: relative;\n    padding-left: 35px;\n    margin-bottom: 12px;\n    cursor: pointer;\n    font-size: 22px;\n    -webkit-user-select: none;\n    -moz-user-select: none;\n    -ms-user-select: none;\n    user-select: none;\n}\n  \n.custom-checkbox input {\n    position: absolute;\n    opacity: 0;\n    cursor: pointer;\n    height: 0;\n    width: 0;\n}\n  \n.checkmark {\n    position: absolute;\n    top: 0;\n    left: 0;\n    height: 25px;\n    width: 25px;\n    background-color: #eee;\n}\n  \n.custom-checkbox:hover input ~ .checkmark {\n    background-color: #ccc;\n}\n  \n.custom-checkbox input:checked ~ .checkmark {\n    background-color: #2196F3;\n}\n  \n.checkmark:after {\n    content: \"\";\n    position: absolute;\n    display: none;\n}\n  \n.custom-checkbox input:checked ~ .checkmark:after {\n    display: block;\n}\n\n.custom-checkbox .checkmark:after {\n    left: 9px;\n    top: 5px;\n    width: 5px;\n    height: 10px;\n    border: solid white;\n    border-width: 0 3px 3px 0;\n    -webkit-transform: rotate(45deg);\n    -ms-transform: rotate(45deg);\n    transform: rotate(45deg);\n}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "./node_modules/css-loader/dist/runtime/api.js":
/*!*****************************************************!*\
  !*** ./node_modules/css-loader/dist/runtime/api.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
// eslint-disable-next-line func-names
module.exports = function (useSourceMap) {
  var list = []; // return the list of modules as css string

  list.toString = function toString() {
    return this.map(function (item) {
      var content = cssWithMappingToString(item, useSourceMap);

      if (item[2]) {
        return "@media ".concat(item[2], " {").concat(content, "}");
      }

      return content;
    }).join('');
  }; // import a list of modules into the list
  // eslint-disable-next-line func-names


  list.i = function (modules, mediaQuery, dedupe) {
    if (typeof modules === 'string') {
      // eslint-disable-next-line no-param-reassign
      modules = [[null, modules, '']];
    }

    var alreadyImportedModules = {};

    if (dedupe) {
      for (var i = 0; i < this.length; i++) {
        // eslint-disable-next-line prefer-destructuring
        var id = this[i][0];

        if (id != null) {
          alreadyImportedModules[id] = true;
        }
      }
    }

    for (var _i = 0; _i < modules.length; _i++) {
      var item = [].concat(modules[_i]);

      if (dedupe && alreadyImportedModules[item[0]]) {
        // eslint-disable-next-line no-continue
        continue;
      }

      if (mediaQuery) {
        if (!item[2]) {
          item[2] = mediaQuery;
        } else {
          item[2] = "".concat(mediaQuery, " and ").concat(item[2]);
        }
      }

      list.push(item);
    }
  };

  return list;
};

function cssWithMappingToString(item, useSourceMap) {
  var content = item[1] || ''; // eslint-disable-next-line prefer-destructuring

  var cssMapping = item[3];

  if (!cssMapping) {
    return content;
  }

  if (useSourceMap && typeof btoa === 'function') {
    var sourceMapping = toComment(cssMapping);
    var sourceURLs = cssMapping.sources.map(function (source) {
      return "/*# sourceURL=".concat(cssMapping.sourceRoot || '').concat(source, " */");
    });
    return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
  }

  return [content].join('\n');
} // Adapted from convert-source-map (MIT)


function toComment(sourceMap) {
  // eslint-disable-next-line no-undef
  var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
  var data = "sourceMappingURL=data:application/json;charset=utf-8;base64,".concat(base64);
  return "/*# ".concat(data, " */");
}

/***/ }),

/***/ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js":
/*!****************************************************************************!*\
  !*** ./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var isOldIE = function isOldIE() {
  var memo;
  return function memorize() {
    if (typeof memo === 'undefined') {
      // Test for IE <= 9 as proposed by Browserhacks
      // @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
      // Tests for existence of standard globals is to allow style-loader
      // to operate correctly into non-standard environments
      // @see https://github.com/webpack-contrib/style-loader/issues/177
      memo = Boolean(window && document && document.all && !window.atob);
    }

    return memo;
  };
}();

var getTarget = function getTarget() {
  var memo = {};
  return function memorize(target) {
    if (typeof memo[target] === 'undefined') {
      var styleTarget = document.querySelector(target); // Special case to return head of iframe instead of iframe itself

      if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {
        try {
          // This will throw an exception if access to iframe is blocked
          // due to cross-origin restrictions
          styleTarget = styleTarget.contentDocument.head;
        } catch (e) {
          // istanbul ignore next
          styleTarget = null;
        }
      }

      memo[target] = styleTarget;
    }

    return memo[target];
  };
}();

var stylesInDom = [];

function getIndexByIdentifier(identifier) {
  var result = -1;

  for (var i = 0; i < stylesInDom.length; i++) {
    if (stylesInDom[i].identifier === identifier) {
      result = i;
      break;
    }
  }

  return result;
}

function modulesToDom(list, options) {
  var idCountMap = {};
  var identifiers = [];

  for (var i = 0; i < list.length; i++) {
    var item = list[i];
    var id = options.base ? item[0] + options.base : item[0];
    var count = idCountMap[id] || 0;
    var identifier = "".concat(id, " ").concat(count);
    idCountMap[id] = count + 1;
    var index = getIndexByIdentifier(identifier);
    var obj = {
      css: item[1],
      media: item[2],
      sourceMap: item[3]
    };

    if (index !== -1) {
      stylesInDom[index].references++;
      stylesInDom[index].updater(obj);
    } else {
      stylesInDom.push({
        identifier: identifier,
        updater: addStyle(obj, options),
        references: 1
      });
    }

    identifiers.push(identifier);
  }

  return identifiers;
}

function insertStyleElement(options) {
  var style = document.createElement('style');
  var attributes = options.attributes || {};

  if (typeof attributes.nonce === 'undefined') {
    var nonce =  true ? __webpack_require__.nc : undefined;

    if (nonce) {
      attributes.nonce = nonce;
    }
  }

  Object.keys(attributes).forEach(function (key) {
    style.setAttribute(key, attributes[key]);
  });

  if (typeof options.insert === 'function') {
    options.insert(style);
  } else {
    var target = getTarget(options.insert || 'head');

    if (!target) {
      throw new Error("Couldn't find a style target. This probably means that the value for the 'insert' parameter is invalid.");
    }

    target.appendChild(style);
  }

  return style;
}

function removeStyleElement(style) {
  // istanbul ignore if
  if (style.parentNode === null) {
    return false;
  }

  style.parentNode.removeChild(style);
}
/* istanbul ignore next  */


var replaceText = function replaceText() {
  var textStore = [];
  return function replace(index, replacement) {
    textStore[index] = replacement;
    return textStore.filter(Boolean).join('\n');
  };
}();

function applyToSingletonTag(style, index, remove, obj) {
  var css = remove ? '' : obj.media ? "@media ".concat(obj.media, " {").concat(obj.css, "}") : obj.css; // For old IE

  /* istanbul ignore if  */

  if (style.styleSheet) {
    style.styleSheet.cssText = replaceText(index, css);
  } else {
    var cssNode = document.createTextNode(css);
    var childNodes = style.childNodes;

    if (childNodes[index]) {
      style.removeChild(childNodes[index]);
    }

    if (childNodes.length) {
      style.insertBefore(cssNode, childNodes[index]);
    } else {
      style.appendChild(cssNode);
    }
  }
}

function applyToTag(style, options, obj) {
  var css = obj.css;
  var media = obj.media;
  var sourceMap = obj.sourceMap;

  if (media) {
    style.setAttribute('media', media);
  } else {
    style.removeAttribute('media');
  }

  if (sourceMap && typeof btoa !== 'undefined') {
    css += "\n/*# sourceMappingURL=data:application/json;base64,".concat(btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))), " */");
  } // For old IE

  /* istanbul ignore if  */


  if (style.styleSheet) {
    style.styleSheet.cssText = css;
  } else {
    while (style.firstChild) {
      style.removeChild(style.firstChild);
    }

    style.appendChild(document.createTextNode(css));
  }
}

var singleton = null;
var singletonCounter = 0;

function addStyle(obj, options) {
  var style;
  var update;
  var remove;

  if (options.singleton) {
    var styleIndex = singletonCounter++;
    style = singleton || (singleton = insertStyleElement(options));
    update = applyToSingletonTag.bind(null, style, styleIndex, false);
    remove = applyToSingletonTag.bind(null, style, styleIndex, true);
  } else {
    style = insertStyleElement(options);
    update = applyToTag.bind(null, style, options);

    remove = function remove() {
      removeStyleElement(style);
    };
  }

  update(obj);
  return function updateStyle(newObj) {
    if (newObj) {
      if (newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap) {
        return;
      }

      update(obj = newObj);
    } else {
      remove();
    }
  };
}

module.exports = function (list, options) {
  options = options || {}; // Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
  // tags it will allow on a page

  if (!options.singleton && typeof options.singleton !== 'boolean') {
    options.singleton = isOldIE();
  }

  list = list || [];
  var lastIdentifiers = modulesToDom(list, options);
  return function update(newList) {
    newList = newList || [];

    if (Object.prototype.toString.call(newList) !== '[object Array]') {
      return;
    }

    for (var i = 0; i < lastIdentifiers.length; i++) {
      var identifier = lastIdentifiers[i];
      var index = getIndexByIdentifier(identifier);
      stylesInDom[index].references--;
    }

    var newLastIdentifiers = modulesToDom(newList, options);

    for (var _i = 0; _i < lastIdentifiers.length; _i++) {
      var _identifier = lastIdentifiers[_i];

      var _index = getIndexByIdentifier(_identifier);

      if (stylesInDom[_index].references === 0) {
        stylesInDom[_index].updater();

        stylesInDom.splice(_index, 1);
      }
    }

    lastIdentifiers = newLastIdentifiers;
  };
};

/***/ }),

/***/ "./src/styles/styles.css":
/*!*******************************!*\
  !*** ./src/styles/styles.css ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(/*! ../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
            var content = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js!./styles.css */ "./node_modules/css-loader/dist/cjs.js!./src/styles/styles.css");

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);



module.exports = content.locals || {};

/***/ }),

/***/ "./src/typescript/app.js":
/*!*******************************!*\
  !*** ./src/typescript/app.js ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var __awaiter = this && this.__awaiter || function (thisArg, _arguments, P, generator) {
  function adopt(value) {
    return value instanceof P ? value : new P(function (resolve) {
      resolve(value);
    });
  }

  return new (P || (P = Promise))(function (resolve, reject) {
    function fulfilled(value) {
      try {
        step(generator.next(value));
      } catch (e) {
        reject(e);
      }
    }

    function rejected(value) {
      try {
        step(generator["throw"](value));
      } catch (e) {
        reject(e);
      }
    }

    function step(result) {
      result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
    }

    step((generator = generator.apply(thisArg, _arguments || [])).next());
  });
};

var __generator = this && this.__generator || function (thisArg, body) {
  var _ = {
    label: 0,
    sent: function () {
      if (t[0] & 1) throw t[1];
      return t[1];
    },
    trys: [],
    ops: []
  },
      f,
      y,
      t,
      g;
  return g = {
    next: verb(0),
    "throw": verb(1),
    "return": verb(2)
  }, typeof Symbol === "function" && (g[Symbol.iterator] = function () {
    return this;
  }), g;

  function verb(n) {
    return function (v) {
      return step([n, v]);
    };
  }

  function step(op) {
    if (f) throw new TypeError("Generator is already executing.");

    while (_) try {
      if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
      if (y = 0, t) op = [op[0] & 2, t.value];

      switch (op[0]) {
        case 0:
        case 1:
          t = op;
          break;

        case 4:
          _.label++;
          return {
            value: op[1],
            done: false
          };

        case 5:
          _.label++;
          y = op[1];
          op = [0];
          continue;

        case 7:
          op = _.ops.pop();

          _.trys.pop();

          continue;

        default:
          if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
            _ = 0;
            continue;
          }

          if (op[0] === 3 && (!t || op[1] > t[0] && op[1] < t[3])) {
            _.label = op[1];
            break;
          }

          if (op[0] === 6 && _.label < t[1]) {
            _.label = t[1];
            t = op;
            break;
          }

          if (t && _.label < t[2]) {
            _.label = t[2];

            _.ops.push(op);

            break;
          }

          if (t[2]) _.ops.pop();

          _.trys.pop();

          continue;
      }

      op = body.call(thisArg, _);
    } catch (e) {
      op = [6, e];
      y = 0;
    } finally {
      f = t = 0;
    }

    if (op[0] & 5) throw op[1];
    return {
      value: op[0] ? op[1] : void 0,
      done: true
    };
  }
};

exports.__esModule = true;
exports.startApp = void 0;

var fightersService_1 = __webpack_require__(/*! ./services/fightersService */ "./src/typescript/services/fightersService.js");

var fightersView_1 = __webpack_require__(/*! ./fightersView */ "./src/typescript/fightersView.js");

var rootElement = document.getElementById('root');
var loadingElement = document.getElementById('loading-overlay');

function startApp() {
  return __awaiter(this, void 0, void 0, function () {
    var fighters, fightersElement, error_1;
    return __generator(this, function (_a) {
      switch (_a.label) {
        case 0:
          _a.trys.push([0, 2, 3, 4]);

          loadingElement === null || loadingElement === void 0 ? void 0 : loadingElement.style.setProperty('visibility', 'visible');
          return [4
          /*yield*/
          , fightersService_1.getFighters()];

        case 1:
          fighters = _a.sent();
          fightersElement = fightersView_1.createFighters(fighters);
          rootElement === null || rootElement === void 0 ? void 0 : rootElement.appendChild(fightersElement);
          return [3
          /*break*/
          , 4];

        case 2:
          error_1 = _a.sent();
          console.warn(error_1);
          rootElement ? rootElement.innerText = 'Failed to load data' : null;
          return [3
          /*break*/
          , 4];

        case 3:
          loadingElement === null || loadingElement === void 0 ? void 0 : loadingElement.style.setProperty('visibility', 'hidden');
          return [7
          /*endfinally*/
          ];

        case 4:
          return [2
          /*return*/
          ];
      }
    });
  });
}

exports.startApp = startApp;

/***/ }),

/***/ "./src/typescript/fight.js":
/*!*********************************!*\
  !*** ./src/typescript/fight.js ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.getBlockPower = exports.getHitPower = exports.getDamage = exports.fight = void 0;

function fight(firstFighter, secondFighter) {
  while (firstFighter.health <= 0 && secondFighter.health <= 0) {
    firstFighter.health -= getDamage(secondFighter, firstFighter);
    secondFighter.health -= getDamage(firstFighter, secondFighter);
  }

  if (firstFighter.health > secondFighter.health) {
    return firstFighter;
  } // return winner


  return secondFighter;
}

exports.fight = fight;

function getDamage(attacker, enemy) {
  return getHitPower(attacker) - getBlockPower(enemy); // damage
}

exports.getDamage = getDamage;

function getHitPower(fighter) {
  var criticalHitChance = getRandomInt(1, 3);
  return fighter.attack * criticalHitChance; // hit power
}

exports.getHitPower = getHitPower;

function getBlockPower(fighter) {
  var dodgeChance = getRandomInt(1, 3);
  return fighter.defense * dodgeChance; // block power
}

exports.getBlockPower = getBlockPower;

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min; // maximum isn`t included, minimum is included
}

/***/ }),

/***/ "./src/typescript/fighterView.js":
/*!***************************************!*\
  !*** ./src/typescript/fighterView.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.createFighter = void 0;

var domHelper_1 = __webpack_require__(/*! ./helpers/domHelper */ "./src/typescript/helpers/domHelper.js");

function createFighter(fighter, handleClick, selectFighter) {
  var name = fighter.name,
      source = fighter.source;
  var nameElement = createName(name);
  var imageElement = createImage(source);
  var checkboxElement = createCheckbox();
  var fighterContainer = domHelper_1.createElement({
    tagName: 'div',
    className: 'fighter'
  });
  fighterContainer.append(imageElement, nameElement, checkboxElement);

  var preventCheckboxClick = function (ev) {
    return ev.stopPropagation();
  };

  var onCheckboxClick = function (ev) {
    return selectFighter(ev, fighter);
  };

  var onFighterClick = function (ev) {
    return handleClick(ev, fighter);
  };

  fighterContainer.addEventListener('click', onFighterClick, false);
  checkboxElement.addEventListener('change', onCheckboxClick, false);
  checkboxElement.addEventListener('click', preventCheckboxClick, false);
  return fighterContainer;
}

exports.createFighter = createFighter;

function createName(name) {
  var nameElement = domHelper_1.createElement({
    tagName: 'span',
    className: 'name'
  });
  nameElement.innerText = name;
  return nameElement;
}

function createImage(source) {
  var attributes = {
    src: source
  };
  var imgElement = domHelper_1.createElement({
    tagName: 'img',
    className: 'fighter-image',
    attributes: attributes
  });
  return imgElement;
}

function createCheckbox() {
  var label = domHelper_1.createElement({
    tagName: 'label',
    className: 'custom-checkbox'
  });
  var span = domHelper_1.createElement({
    tagName: 'span',
    className: 'checkmark'
  });
  var attributes = {
    type: 'checkbox'
  };
  var checkboxElement = domHelper_1.createElement({
    tagName: 'input',
    attributes: attributes
  });
  label.append(checkboxElement, span);
  return label;
}

/***/ }),

/***/ "./src/typescript/fightersView.js":
/*!****************************************!*\
  !*** ./src/typescript/fightersView.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var __awaiter = this && this.__awaiter || function (thisArg, _arguments, P, generator) {
  function adopt(value) {
    return value instanceof P ? value : new P(function (resolve) {
      resolve(value);
    });
  }

  return new (P || (P = Promise))(function (resolve, reject) {
    function fulfilled(value) {
      try {
        step(generator.next(value));
      } catch (e) {
        reject(e);
      }
    }

    function rejected(value) {
      try {
        step(generator["throw"](value));
      } catch (e) {
        reject(e);
      }
    }

    function step(result) {
      result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
    }

    step((generator = generator.apply(thisArg, _arguments || [])).next());
  });
};

var __generator = this && this.__generator || function (thisArg, body) {
  var _ = {
    label: 0,
    sent: function () {
      if (t[0] & 1) throw t[1];
      return t[1];
    },
    trys: [],
    ops: []
  },
      f,
      y,
      t,
      g;
  return g = {
    next: verb(0),
    "throw": verb(1),
    "return": verb(2)
  }, typeof Symbol === "function" && (g[Symbol.iterator] = function () {
    return this;
  }), g;

  function verb(n) {
    return function (v) {
      return step([n, v]);
    };
  }

  function step(op) {
    if (f) throw new TypeError("Generator is already executing.");

    while (_) try {
      if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
      if (y = 0, t) op = [op[0] & 2, t.value];

      switch (op[0]) {
        case 0:
        case 1:
          t = op;
          break;

        case 4:
          _.label++;
          return {
            value: op[1],
            done: false
          };

        case 5:
          _.label++;
          y = op[1];
          op = [0];
          continue;

        case 7:
          op = _.ops.pop();

          _.trys.pop();

          continue;

        default:
          if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
            _ = 0;
            continue;
          }

          if (op[0] === 3 && (!t || op[1] > t[0] && op[1] < t[3])) {
            _.label = op[1];
            break;
          }

          if (op[0] === 6 && _.label < t[1]) {
            _.label = t[1];
            t = op;
            break;
          }

          if (t && _.label < t[2]) {
            _.label = t[2];

            _.ops.push(op);

            break;
          }

          if (t[2]) _.ops.pop();

          _.trys.pop();

          continue;
      }

      op = body.call(thisArg, _);
    } catch (e) {
      op = [6, e];
      y = 0;
    } finally {
      f = t = 0;
    }

    if (op[0] & 5) throw op[1];
    return {
      value: op[0] ? op[1] : void 0,
      done: true
    };
  }
};

exports.__esModule = true;
exports.getFighterInfo = exports.createFighters = void 0;

var fighterView_1 = __webpack_require__(/*! ./fighterView */ "./src/typescript/fighterView.js");

var fighterDetails_1 = __webpack_require__(/*! ./modals/fighterDetails */ "./src/typescript/modals/fighterDetails.js");

var domHelper_1 = __webpack_require__(/*! ./helpers/domHelper */ "./src/typescript/helpers/domHelper.js");

var fight_1 = __webpack_require__(/*! ./fight */ "./src/typescript/fight.js");

var winner_1 = __webpack_require__(/*! ./modals/winner */ "./src/typescript/modals/winner.js");

var fightersService_1 = __webpack_require__(/*! ./services/fightersService */ "./src/typescript/services/fightersService.js");

function createFighters(fighters) {
  var selectFighterForBattle = createFightersSelector();
  var fighterElements = fighters.map(function (fighter) {
    return fighterView_1.createFighter(fighter, showFighterDetails, selectFighterForBattle);
  });
  var fightersContainer = domHelper_1.createElement({
    tagName: 'div',
    className: 'fighters'
  });
  fightersContainer.append.apply(fightersContainer, fighterElements);
  return fightersContainer;
}

exports.createFighters = createFighters;
var fightersDetailsCache = new Map();

function showFighterDetails(event, fighter) {
  return __awaiter(this, void 0, void 0, function () {
    var fullInfo;
    return __generator(this, function (_a) {
      switch (_a.label) {
        case 0:
          return [4
          /*yield*/
          , getFighterInfo(fighter._id)];

        case 1:
          fullInfo = _a.sent();
          fighterDetails_1.showFighterDetailsModal(fullInfo);
          return [2
          /*return*/
          ];
      }
    });
  });
}

function getFighterInfo(fighterId) {
  return __awaiter(this, void 0, void 0, function () {
    var fighter;
    return __generator(this, function (_a) {
      switch (_a.label) {
        case 0:
          return [4
          /*yield*/
          , fightersService_1.getFighterDetails(fighterId)];

        case 1:
          fighter = _a.sent();
          return [2
          /*return*/
          , fighter];
      }
    });
  });
}

exports.getFighterInfo = getFighterInfo;

function createFightersSelector() {
  var selectedFighters = new Map();
  return function selectFighterForBattle(event, fighter) {
    return __awaiter(this, void 0, void 0, function () {
      var fullInfo, fighters, winner;
      return __generator(this, function (_a) {
        switch (_a.label) {
          case 0:
            return [4
            /*yield*/
            , getFighterInfo(fighter._id)];

          case 1:
            fullInfo = _a.sent();

            if (event.target.checked) {
              selectedFighters.set(fighter._id, fullInfo);
            } else {
              selectedFighters["delete"](fighter._id);
            }

            if (selectedFighters.size === 2) {
              fighters = selectedFighters.values();
              winner = fight_1.fight(fighters.next().value, fighters.next().value);
              winner_1.showWinnerModal(winner);
            }

            return [2
            /*return*/
            ];
        }
      });
    });
  };
}

/***/ }),

/***/ "./src/typescript/helpers/apiHelper.js":
/*!*********************************************!*\
  !*** ./src/typescript/helpers/apiHelper.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var __awaiter = this && this.__awaiter || function (thisArg, _arguments, P, generator) {
  function adopt(value) {
    return value instanceof P ? value : new P(function (resolve) {
      resolve(value);
    });
  }

  return new (P || (P = Promise))(function (resolve, reject) {
    function fulfilled(value) {
      try {
        step(generator.next(value));
      } catch (e) {
        reject(e);
      }
    }

    function rejected(value) {
      try {
        step(generator["throw"](value));
      } catch (e) {
        reject(e);
      }
    }

    function step(result) {
      result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
    }

    step((generator = generator.apply(thisArg, _arguments || [])).next());
  });
};

var __generator = this && this.__generator || function (thisArg, body) {
  var _ = {
    label: 0,
    sent: function () {
      if (t[0] & 1) throw t[1];
      return t[1];
    },
    trys: [],
    ops: []
  },
      f,
      y,
      t,
      g;
  return g = {
    next: verb(0),
    "throw": verb(1),
    "return": verb(2)
  }, typeof Symbol === "function" && (g[Symbol.iterator] = function () {
    return this;
  }), g;

  function verb(n) {
    return function (v) {
      return step([n, v]);
    };
  }

  function step(op) {
    if (f) throw new TypeError("Generator is already executing.");

    while (_) try {
      if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
      if (y = 0, t) op = [op[0] & 2, t.value];

      switch (op[0]) {
        case 0:
        case 1:
          t = op;
          break;

        case 4:
          _.label++;
          return {
            value: op[1],
            done: false
          };

        case 5:
          _.label++;
          y = op[1];
          op = [0];
          continue;

        case 7:
          op = _.ops.pop();

          _.trys.pop();

          continue;

        default:
          if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
            _ = 0;
            continue;
          }

          if (op[0] === 3 && (!t || op[1] > t[0] && op[1] < t[3])) {
            _.label = op[1];
            break;
          }

          if (op[0] === 6 && _.label < t[1]) {
            _.label = t[1];
            t = op;
            break;
          }

          if (t && _.label < t[2]) {
            _.label = t[2];

            _.ops.push(op);

            break;
          }

          if (t[2]) _.ops.pop();

          _.trys.pop();

          continue;
      }

      op = body.call(thisArg, _);
    } catch (e) {
      op = [6, e];
      y = 0;
    } finally {
      f = t = 0;
    }

    if (op[0] & 5) throw op[1];
    return {
      value: op[0] ? op[1] : void 0,
      done: true
    };
  }
};

exports.__esModule = true;
exports.callApi = void 0;

var mockData_1 = __webpack_require__(/*! ./mockData */ "./src/typescript/helpers/mockData.js");

var API_URL = 'https://api.github.com/repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/';
var useMockAPI = true;

function callApi(endpoint, method) {
  return __awaiter(this, void 0, void 0, function () {
    var url, options;
    return __generator(this, function (_a) {
      url = API_URL + endpoint;
      options = {
        method: method
      };
      return [2
      /*return*/
      , useMockAPI ? fakeCallApi(endpoint) : fetch(url, options).then(function (response) {
        return response.ok ? response.json() : Promise.reject(Error('Failed to load'));
      }).then(function (result) {
        return JSON.parse(atob(result.content));
      })["catch"](function (error) {
        throw error;
      })];
    });
  });
}

exports.callApi = callApi;

function fakeCallApi(endpoint) {
  return __awaiter(this, void 0, void 0, function () {
    var response;
    return __generator(this, function (_a) {
      response = endpoint === 'fighters.json' ? mockData_1.fighters : getFighterById(endpoint);
      return [2
      /*return*/
      , new Promise(function (resolve, reject) {
        setTimeout(function () {
          return response ? resolve(response) : reject(Error('Failed to load'));
        }, 500);
      })];
    });
  });
}

function getFighterById(endpoint) {
  var start = endpoint.lastIndexOf('/');
  var end = endpoint.lastIndexOf('.json');
  var id = endpoint.substring(start + 1, end);
  return mockData_1.fightersDetails.find(function (it) {
    return it._id === id;
  });
}

/***/ }),

/***/ "./src/typescript/helpers/domHelper.js":
/*!*********************************************!*\
  !*** ./src/typescript/helpers/domHelper.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.createElement = void 0;

function createElement(x) {
  var element = document.createElement(x.tagName);

  if (x.className) {
    element.classList.add(x.className);
  }

  if (x.attributes) {
    Object.keys(x.attributes).forEach(function (key) {
      return element.setAttribute(key, x.attributes[key]);
    });
  }

  return element;
}

exports.createElement = createElement;

/***/ }),

/***/ "./src/typescript/helpers/mockData.js":
/*!********************************************!*\
  !*** ./src/typescript/helpers/mockData.js ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.fightersDetails = exports.fighters = void 0;
exports.fighters = [{
  "_id": "1",
  "name": "Ryu",
  "source": "https://media.giphy.com/media/kdHa4JvihB2gM/giphy.gif"
}, {
  "_id": "2",
  "name": "Dhalsim",
  "source": "https://i.pinimg.com/originals/c0/53/f2/c053f2bce4d2375fee8741acfb35d44d.gif"
}, {
  "_id": "3",
  "name": "Guile",
  "source": "https://66.media.tumblr.com/tumblr_lq8g3548bC1qd0wh3o1_400.gif"
}, {
  "_id": "4",
  "name": "Zangief",
  "source": "https://media1.giphy.com/media/nlbIvY9K0jfAA/source.gif"
}, {
  "_id": "5",
  "name": "Ken",
  "source": "https://i.pinimg.com/originals/46/4b/36/464b36a7aecd988e3c51e56a823dbedc.gif"
}, {
  "_id": "6",
  "name": "Bison",
  "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/bison-hdstance.gif"
}];
exports.fightersDetails = [{
  "_id": "1",
  "name": "Ryu",
  "health": 45,
  "attack": 4,
  "defense": 3,
  "source": "https://media.giphy.com/media/kdHa4JvihB2gM/giphy.gif"
}, {
  "_id": "2",
  "name": "Dhalsim",
  "health": 60,
  "attack": 3,
  "defense": 1,
  "source": "https://i.pinimg.com/originals/c0/53/f2/c053f2bce4d2375fee8741acfb35d44d.gif"
}, {
  "_id": "3",
  "name": "Guile",
  "health": 45,
  "attack": 4,
  "defense": 3,
  "source": "https://66.media.tumblr.com/tumblr_lq8g3548bC1qd0wh3o1_400.gif"
}, {
  "_id": "4",
  "name": "Zangief",
  "health": 60,
  "attack": 4,
  "defense": 1,
  "source": "https://media1.giphy.com/media/nlbIvY9K0jfAA/source.gif"
}, {
  "_id": "5",
  "name": "Ken",
  "health": 45,
  "attack": 3,
  "defense": 4,
  "source": "https://i.pinimg.com/originals/46/4b/36/464b36a7aecd988e3c51e56a823dbedc.gif"
}, {
  "_id": "6",
  "name": "Bison",
  "health": 45,
  "attack": 5,
  "defense": 4,
  "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/bison-hdstance.gif"
}];

/***/ }),

/***/ "./src/typescript/modals/fighterDetails.js":
/*!*************************************************!*\
  !*** ./src/typescript/modals/fighterDetails.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.showFighterDetailsModal = void 0;

var domHelper_1 = __webpack_require__(/*! ../helpers/domHelper */ "./src/typescript/helpers/domHelper.js");

var modal_1 = __webpack_require__(/*! ./modal */ "./src/typescript/modals/modal.js");

function showFighterDetailsModal(fighter) {
  var title = 'Fighter info';
  var bodyElement = createFighterDetails(fighter);
  modal_1.showModal({
    title: title,
    bodyElement: bodyElement
  });
}

exports.showFighterDetailsModal = showFighterDetailsModal;

function createFighterDetails(fighter) {
  var name = fighter.name;
  var fighterDetails = domHelper_1.createElement({
    tagName: 'div',
    className: 'modal-body'
  });
  var nameElement = domHelper_1.createElement({
    tagName: 'span',
    className: 'fighter-name'
  }); // show fighter name, attack, defense, health, image

  nameElement.innerText = name;
  fighterDetails.append(nameElement);
  return fighterDetails;
}

/***/ }),

/***/ "./src/typescript/modals/modal.js":
/*!****************************************!*\
  !*** ./src/typescript/modals/modal.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.showModal = void 0;

var domHelper_1 = __webpack_require__(/*! ../helpers/domHelper */ "./src/typescript/helpers/domHelper.js");

function showModal(x) {
  var root = getModalContainer();
  var modal = createModal(x.title, x.bodyElement);
  root === null || root === void 0 ? void 0 : root.append(modal);
}

exports.showModal = showModal;

function getModalContainer() {
  return document.getElementById('root');
}

function createModal(title, bodyElement) {
  var layer = domHelper_1.createElement({
    tagName: 'div',
    className: 'modal-layer'
  });
  var modalContainer = domHelper_1.createElement({
    tagName: 'div',
    className: 'modal-root'
  });
  var header = createHeader(title);
  modalContainer.append(header, bodyElement);
  layer.append(modalContainer);
  return layer;
}

function createHeader(title) {
  var headerElement = domHelper_1.createElement({
    tagName: 'div',
    className: 'modal-header'
  });
  var titleElement = domHelper_1.createElement({
    tagName: 'span'
  });
  var closeButton = domHelper_1.createElement({
    tagName: 'div',
    className: 'close-btn'
  });
  titleElement.innerText = title;
  closeButton.innerText = '×';
  closeButton.addEventListener('click', hideModal);
  headerElement.append(title, closeButton);
  return headerElement;
}

function hideModal(event) {
  var modal = document.getElementsByClassName('modal-layer')[0];
  modal === null || modal === void 0 ? void 0 : modal.remove();
}

/***/ }),

/***/ "./src/typescript/modals/winner.js":
/*!*****************************************!*\
  !*** ./src/typescript/modals/winner.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.showWinnerModal = void 0;

function showWinnerModal(fighter) {// show winner name and image
}

exports.showWinnerModal = showWinnerModal;

/***/ }),

/***/ "./src/typescript/services/fightersService.js":
/*!****************************************************!*\
  !*** ./src/typescript/services/fightersService.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var __awaiter = this && this.__awaiter || function (thisArg, _arguments, P, generator) {
  function adopt(value) {
    return value instanceof P ? value : new P(function (resolve) {
      resolve(value);
    });
  }

  return new (P || (P = Promise))(function (resolve, reject) {
    function fulfilled(value) {
      try {
        step(generator.next(value));
      } catch (e) {
        reject(e);
      }
    }

    function rejected(value) {
      try {
        step(generator["throw"](value));
      } catch (e) {
        reject(e);
      }
    }

    function step(result) {
      result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
    }

    step((generator = generator.apply(thisArg, _arguments || [])).next());
  });
};

var __generator = this && this.__generator || function (thisArg, body) {
  var _ = {
    label: 0,
    sent: function () {
      if (t[0] & 1) throw t[1];
      return t[1];
    },
    trys: [],
    ops: []
  },
      f,
      y,
      t,
      g;
  return g = {
    next: verb(0),
    "throw": verb(1),
    "return": verb(2)
  }, typeof Symbol === "function" && (g[Symbol.iterator] = function () {
    return this;
  }), g;

  function verb(n) {
    return function (v) {
      return step([n, v]);
    };
  }

  function step(op) {
    if (f) throw new TypeError("Generator is already executing.");

    while (_) try {
      if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
      if (y = 0, t) op = [op[0] & 2, t.value];

      switch (op[0]) {
        case 0:
        case 1:
          t = op;
          break;

        case 4:
          _.label++;
          return {
            value: op[1],
            done: false
          };

        case 5:
          _.label++;
          y = op[1];
          op = [0];
          continue;

        case 7:
          op = _.ops.pop();

          _.trys.pop();

          continue;

        default:
          if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
            _ = 0;
            continue;
          }

          if (op[0] === 3 && (!t || op[1] > t[0] && op[1] < t[3])) {
            _.label = op[1];
            break;
          }

          if (op[0] === 6 && _.label < t[1]) {
            _.label = t[1];
            t = op;
            break;
          }

          if (t && _.label < t[2]) {
            _.label = t[2];

            _.ops.push(op);

            break;
          }

          if (t[2]) _.ops.pop();

          _.trys.pop();

          continue;
      }

      op = body.call(thisArg, _);
    } catch (e) {
      op = [6, e];
      y = 0;
    } finally {
      f = t = 0;
    }

    if (op[0] & 5) throw op[1];
    return {
      value: op[0] ? op[1] : void 0,
      done: true
    };
  }
};

exports.__esModule = true;
exports.getFighterDetails = exports.getFighters = void 0;

var apiHelper_1 = __webpack_require__(/*! ../helpers/apiHelper */ "./src/typescript/helpers/apiHelper.js");

function getFighters() {
  return __awaiter(this, void 0, void 0, function () {
    var endpoint, apiResult, error_1;
    return __generator(this, function (_a) {
      switch (_a.label) {
        case 0:
          _a.trys.push([0, 2,, 3]);

          endpoint = 'fighters.json';
          return [4
          /*yield*/
          , apiHelper_1.callApi(endpoint, 'GET')];

        case 1:
          apiResult = _a.sent();
          return [2
          /*return*/
          , apiResult];

        case 2:
          error_1 = _a.sent();
          throw error_1;

        case 3:
          return [2
          /*return*/
          ];
      }
    });
  });
}

exports.getFighters = getFighters;

function getFighterDetails(id) {
  return __awaiter(this, void 0, void 0, function () {
    var endpoint, apiResult, e_1;
    return __generator(this, function (_a) {
      switch (_a.label) {
        case 0:
          _a.trys.push([0, 2,, 3]);

          endpoint = "/" + id + ".json";
          return [4
          /*yield*/
          , apiHelper_1.callApi(endpoint, 'GET')];

        case 1:
          apiResult = _a.sent();
          console.log(id, apiResult);
          return [2
          /*return*/
          , apiResult];

        case 2:
          e_1 = _a.sent();
          throw e_1;

        case 3:
          return [2
          /*return*/
          ];
      }
    });
  });
}

exports.getFighterDetails = getFighterDetails;

/***/ })

/******/ });
//# sourceMappingURL=bundle.js.map