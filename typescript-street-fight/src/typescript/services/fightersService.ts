import { Fighter } from '../../../resources/api/details/fighter/boxer.interface';
import { callApi } from '../helpers/apiHelper';

export async function getFighters() {
  try {
    const endpoint = 'fighters.json';
    const apiResult = await callApi(endpoint, 'GET');
    
    return apiResult as Fighter[];
  } catch (error) {
    throw error;
  }
}

export async function getFighterDetails(id: string): Promise<Fighter> {
  
  try {
    const endpoint = `/${id}.json`;
    const apiResult = await callApi(endpoint, 'GET')
    console.log(id, apiResult)
    return apiResult as Fighter;
  } catch (e) {
    throw e;
  }
}

