import { createFighter } from './fighterView';
import { showFighterDetailsModal } from './modals/fighterDetails';
import { createElement } from './helpers/domHelper';
import { fight } from './fight';
import { showWinnerModal } from './modals/winner';
import { Fighter } from '../../resources/api/details/fighter/boxer.interface';
import { getFighterDetails } from './services/fightersService';

export function createFighters(fighters: Fighter[]) {
  const selectFighterForBattle = createFightersSelector();
  const fighterElements = fighters.map(fighter => createFighter(fighter, showFighterDetails, selectFighterForBattle));
  const fightersContainer = createElement({ tagName: 'div', className: 'fighters' });

  fightersContainer.append(...fighterElements);

  return fightersContainer;
}

const fightersDetailsCache = new Map();

async function showFighterDetails(event: Event, fighter: Fighter) {
  const fullInfo = await getFighterInfo(fighter._id);
  showFighterDetailsModal(fullInfo);
}

export async function getFighterInfo(fighterId: string) {
  // get fighter form fightersDetailsCache or use getFighterDetails function
  let fighter = await getFighterDetails(fighterId);
  return fighter;
}

function createFightersSelector() {
  const selectedFighters = new Map<string, any>();

  return async function selectFighterForBattle(event: Event, fighter: Fighter) {
    const fullInfo = await getFighterInfo(fighter._id);

    if (((event.target) as HTMLInputElement).checked) {
      selectedFighters.set(fighter._id, fullInfo);
    } else { 
      selectedFighters.delete(fighter._id);
    }

    if (selectedFighters.size === 2) {
      let fighters = selectedFighters.values();
      const winner = fight(fighters.next().value, fighters.next().value);
      showWinnerModal(winner)
    }
  }
}
