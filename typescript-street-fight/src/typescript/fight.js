"use strict";
exports.__esModule = true;
exports.getBlockPower = exports.getHitPower = exports.getDamage = exports.fight = void 0;
function fight(firstFighter, secondFighter) {
    while (firstFighter.health <= 0 && secondFighter.health <= 0) {
        firstFighter.health -= getDamage(secondFighter, firstFighter);
        secondFighter.health -= getDamage(firstFighter, secondFighter);
    }
    if (firstFighter.health > secondFighter.health) {
        return firstFighter;
    } // return winner
    return secondFighter;
}
exports.fight = fight;
function getDamage(attacker, enemy) {
    return getHitPower(attacker) - getBlockPower(enemy); // damage
}
exports.getDamage = getDamage;
function getHitPower(fighter) {
    var criticalHitChance = getRandomInt(1, 3);
    return fighter.attack * criticalHitChance; // hit power
}
exports.getHitPower = getHitPower;
function getBlockPower(fighter) {
    var dodgeChance = getRandomInt(1, 3);
    return fighter.defense * dodgeChance; // block power
}
exports.getBlockPower = getBlockPower;
function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; // maximum isn`t included, minimum is included
}
