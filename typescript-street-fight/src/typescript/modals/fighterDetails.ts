import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
import { Fighter } from '../../../resources/api/details/fighter/boxer.interface';
import { fighters } from '../helpers/mockData';

export  function showFighterDetailsModal(fighter: Fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter: Fighter) {
  const { name, health, attack, defense} = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'h4', className: 'fighter-name' });
  const healthElement = createElement({tagName: 'span', className: 'fighter-health'});
  const attackElement = createElement({tagName: 'span', className: 'fighter-attack'});
  const defenseElement = createElement({tagName: 'span', className: 'fighter-defense'});

  nameElement.innerText = name;
  healthElement.innerText = '\n health: ' + health;
  attackElement.innerText = '\n attack: ' + attack;
  defenseElement.innerText = '\n defense: ' + defense;
  fighterDetails.append(nameElement, healthElement, attackElement, defenseElement);

  return fighterDetails;
}
