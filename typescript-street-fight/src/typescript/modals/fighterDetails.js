"use strict";
exports.__esModule = true;
exports.showFighterDetailsModal = void 0;
var domHelper_1 = require("../helpers/domHelper");
var modal_1 = require("./modal");
function showFighterDetailsModal(fighter) {
    var title = 'Fighter info';
    var bodyElement = createFighterDetails(fighter);
    modal_1.showModal({ title: title, bodyElement: bodyElement });
}
exports.showFighterDetailsModal = showFighterDetailsModal;
function createFighterDetails(fighter) {
    var name = fighter.name, health = fighter.health, attack = fighter.attack, defense = fighter.defense;
    var fighterDetails = domHelper_1.createElement({ tagName: 'div', className: 'modal-body' });
    var nameElement = domHelper_1.createElement({ tagName: 'h4', className: 'fighter-name' });
    var healthElement = domHelper_1.createElement({ tagName: 'span', className: 'fighter-health' });
    var attackElement = domHelper_1.createElement({ tagName: 'span', className: 'fighter-attack' });
    var defenseElement = domHelper_1.createElement({ tagName: 'span', className: 'fighter-defense' });
    nameElement.innerText = name;
    healthElement.innerText = '\n health: ' + health;
    attackElement.innerText = '\n attack: ' + attack;
    defenseElement.innerText = '\n defense: ' + defense;
    fighterDetails.append(nameElement, healthElement, attackElement, defenseElement);
    return fighterDetails;
}
