"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.getFighterInfo = exports.createFighters = void 0;
var fighterView_1 = require("./fighterView");
var fighterDetails_1 = require("./modals/fighterDetails");
var domHelper_1 = require("./helpers/domHelper");
var fight_1 = require("./fight");
var winner_1 = require("./modals/winner");
var fightersService_1 = require("./services/fightersService");
function createFighters(fighters) {
    var selectFighterForBattle = createFightersSelector();
    var fighterElements = fighters.map(function (fighter) { return fighterView_1.createFighter(fighter, showFighterDetails, selectFighterForBattle); });
    var fightersContainer = domHelper_1.createElement({ tagName: 'div', className: 'fighters' });
    fightersContainer.append.apply(fightersContainer, fighterElements);
    return fightersContainer;
}
exports.createFighters = createFighters;
var fightersDetailsCache = new Map();
function showFighterDetails(event, fighter) {
    return __awaiter(this, void 0, void 0, function () {
        var fullInfo;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, getFighterInfo(fighter._id)];
                case 1:
                    fullInfo = _a.sent();
                    fighterDetails_1.showFighterDetailsModal(fullInfo);
                    return [2 /*return*/];
            }
        });
    });
}
function getFighterInfo(fighterId) {
    return __awaiter(this, void 0, void 0, function () {
        var fighter;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, fightersService_1.getFighterDetails(fighterId)];
                case 1:
                    fighter = _a.sent();
                    return [2 /*return*/, fighter];
            }
        });
    });
}
exports.getFighterInfo = getFighterInfo;
function createFightersSelector() {
    var selectedFighters = new Map();
    return function selectFighterForBattle(event, fighter) {
        return __awaiter(this, void 0, void 0, function () {
            var fullInfo, fighters, winner;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, getFighterInfo(fighter._id)];
                    case 1:
                        fullInfo = _a.sent();
                        if ((event.target).checked) {
                            selectedFighters.set(fighter._id, fullInfo);
                        }
                        else {
                            selectedFighters["delete"](fighter._id);
                        }
                        if (selectedFighters.size === 2) {
                            fighters = selectedFighters.values();
                            winner = fight_1.fight(fighters.next().value, fighters.next().value);
                            winner_1.showWinnerModal(winner);
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
}
