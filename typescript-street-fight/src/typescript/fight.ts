import { Fighter } from "../../resources/api/details/fighter/boxer.interface";

export function fight(firstFighter: Fighter, secondFighter: Fighter) {
  
  while (firstFighter.health <= 0 && secondFighter.health <= 0) {
    firstFighter.health -= getDamage(secondFighter, firstFighter);
    secondFighter.health -= getDamage(firstFighter, secondFighter);
  }
  
  if (firstFighter.health > secondFighter.health) {
    return firstFighter;
  }                                               // return winner
  return secondFighter;
}

export function getDamage(attacker: Fighter, enemy: Fighter) { 
  return getHitPower(attacker) - getBlockPower(enemy); // damage
}

export function getHitPower(fighter: Fighter) { 
  let criticalHitChance = getRandomInt(1, 3);
  return fighter.attack * criticalHitChance; // hit power
}

export function getBlockPower(fighter: Fighter) {  
  let dodgeChance = getRandomInt(1, 3);
  return fighter.defense * dodgeChance; // block power
}

function getRandomInt(min: number, max: number) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min; // maximum isn`t included, minimum is included
}
