"use strict";
exports.__esModule = true;
exports.createElement = void 0;
function createElement(x) {
    var element = document.createElement(x.tagName);
    if (x.className) {
        element.classList.add(x.className);
    }
    if (x.attributes) {
        Object.keys(x.attributes).forEach(function (key) { return element.setAttribute(key, x.attributes[key]); });
    }
    return element;
}
exports.createElement = createElement;
