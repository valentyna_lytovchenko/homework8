export function createElement(x: { tagName: string, className?: string, attributes?: any }) {
  const element = document.createElement(x.tagName);
  
  if (x.className) {
    element.classList.add(x.className);
  }
  if(x.attributes) {
    Object.keys(x.attributes).forEach(key => element.setAttribute(key, x.attributes[key]));
  }

  return element;
}